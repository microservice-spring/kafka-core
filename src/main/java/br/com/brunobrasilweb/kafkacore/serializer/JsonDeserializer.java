package br.com.brunobrasilweb.kafkacore.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonDeserializer<T> extends org.springframework.kafka.support.serializer.JsonDeserializer<T> {
    private static final Logger log = LoggerFactory.getLogger(JsonDeserializer.class);

    public JsonDeserializer(Class<? super T> targetType, ObjectMapper objectMapper) {
        super(targetType, objectMapper, false);
        super.addTrustedPackages(new String[]{"br.com.brunobrasilweb.*"});
    }

    public T deserialize(String topic, Headers headers, byte[] data) {
        try {
            return super.deserialize(topic, headers, data);
        } catch (Exception var5) {
            log.error("Error on deserialization", var5);
            return null;
        }
    }

    public T deserialize(String topic, byte[] data) {
        try {
            return super.deserialize(topic, data);
        } catch (Exception var4) {
            log.error("Error on deserialization", var4);
            return null;
        }
    }
}
