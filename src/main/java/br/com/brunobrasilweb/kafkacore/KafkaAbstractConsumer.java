package br.com.brunobrasilweb.kafkacore;

import br.com.brunobrasilweb.kafkacore.serializer.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.HashMap;
import java.util.Map;

public abstract class KafkaAbstractConsumer<T> {

    @Autowired
    private ObjectMapper objectMapper;

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Value(value = "${spring.kafka.consumer.group-id}")
    private String groupId;

    protected ConsumerFactory<String, Object> buildConsumerFactory(Class<T> clazz) {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        StringDeserializer keyDeserializer = new StringDeserializer();
        JsonDeserializer<T> valueDeserializer = new JsonDeserializer(clazz, this.objectMapper);

        return new DefaultKafkaConsumerFactory(props, keyDeserializer, valueDeserializer);
    }

}
